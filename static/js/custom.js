$(document).ready(function() {
    $(function () {
        var date = new Date();
        date.setDate(date.getDate() + 1);

        $('#datetimepicker1').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            maxViewMode: 0,
            autoclose: true,
            todayHighlight: true
        });
    });

    $("#addrecordform").submit(function (e) {

        e.preventDefault();
        var form = $("#addrecordform").serializeJSON();
        // var fdata = getFormData($form);

        $.ajax({
            type: "POST",
            url: "/add",
            contentType: "application/JSON",
            data: JSON.stringify(form),
            // data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                window.location = '/';
            }
        });
    });

    $("#add_new_user").submit(function (e) {
        e.preventDefault();

        var form = $("#add_new_user").serializeJSON();

        $.ajax({
            type: "POST",
            url: "/add-new-user",
            contentType: "application/JSON",
            data: JSON.stringify(form),
            success: function(data) {
                window.location = '/members';
            }
        })
    })

    $('#disable_date').tooltip();
    $('#rtr_iface').tooltip();
});

function deluser(username) {
    $.ajax({
        type: "POST",
        url: "/del-user",
        contentType: "application/JSON",
        data: JSON.stringify({'username': username}),
        success: function(data) {
            window.location = '/members';
        }
    })
}

function delservice(service_id) {
    $.ajax({
        type: "POST",
        url: "/del-service",
        contentType: "application/JSON",
        data: JSON.stringify({'service_id': service_id}),
        success: function(data) {
            window.location = '/';
        }
    })
}

