import click
import re
import time
from executor import Executor
# from datetime import datetime as dt
from database import db_list_entries, db_add_new_entry, db_remove_id


def menu():
    menu = "main"
    while 1:
        if menu == "main":
            click.echo("Menu:")
            click.echo(" [W]: wyswietl uslugi zaplanowane do wylaczenia")
            click.echo(" [P]: wyswietl wszystkie wpisy w bazie")
            click.echo(" [D]: dodaj nowy wpis")
            click.echo(" [U]: usun wpis z bazy")
            click.echo(" [Q]: wyjdz")
            char = click.getchar()
            try:
                MENU_OPTIONS[char.lower()]()
            except KeyError:
                click.echo(click.style("Nieprawidlowy wybor", fg="red"))


def list_entries():
    db_list_entries()


def list_all_entries():
    db_list_entries(future=False)


def validate_date(date):
    if not re.match(r'\d{4}-\d{1,2}-\d{1,2}', date):
        raise click.BadParameter('Nieprawidlowo data', param=date)
    current_date = int(time.time())
    user_date = time.mktime(time.strptime(date + " 00:00:01", '%Y-%m-%d %H:%M:%S'))
    if user_date < current_date:
        raise click.BadParameter('Wprowadzona data jest z przeszlosci lub dzisiejsza - wprowadz prawidlowa date')
    return date


def add_new_entry():
    try:
        date = click.prompt('Podaj date wylaczenia (w formacie YYYY-MM-DD)',
                            value_proc=validate_date)
        router = click.prompt('Podaj router')
        iface = click.prompt('Podaj interfejs z unitem (np. ae1.111)')
        serviceid = click.prompt('Podaj ID uslugi')
        logicalsystem = click.prompt('Podaj logiczny system (lub Enter bez LS)',
                                     default='-')
    except click.Abort:
        menu()

    click.echo("*"*50)
    click.echo("Wprowadzone dane:")
    click.echo("Data wylaczenia: " + click.style(date, fg="green"))
    click.echo("Router: " + click.style(router, fg="green"))
    click.echo("Logiczny system: " + click.style(logicalsystem, fg="green"))
    click.echo("Interfejs: " + click.style(iface, fg="green"))
    click.echo("ID uslugi: " + click.style(serviceid, fg="green"))
    click.echo("*"*50)
    click.echo("Pobieram opis interfejsu z routera - prosze czekac...")
    e = Executor()
    rtr_service_desc = e.get_iface_desc(router, iface)
    click.echo("Opis uslugi z routera: " + rtr_service_desc)
    if click.confirm('Czy dane sa prawidlowe?', abort=False):
        db_add_new_entry(date, router, iface, logicalsystem, serviceid)
        click.echo(click.style("Dodano nowy wpis", fg="green"))


def remove_entry():
    try:
        del_id = click.prompt('Podaj nr rekordu ktory ma zostac usuniety z bazy: ')
    except click.Abort:
        menu()

    result = db_remove_id(int(del_id))
    if result['status'] == 'success':
        click.echo(click.style(result['message'], fg="green"))
    else:
        click.echo(click.style(result['message'], fg="red"))


def edit_entry():
    print('To-do')


MENU_OPTIONS = {'w': list_entries,
                'p': list_all_entries,
                'd': add_new_entry,
                'u': remove_entry,
                'e': edit_entry,
                'q': quit}


if __name__ == '__main__':
    menu()
