import time
import sys
import os
import sqlite3 as sql

R = "\033[0;31;40m"  # RED
G = "\033[0;32;40m"  # GREEN
Y = "\033[1;33m"  # Yellow
B = "\033[0;34;40m"  # Blue
N = "\033[0m"  # Reset


class db(object):
    def __init__(self):
        path = os.path.dirname(os.path.abspath(__file__))
        db_file = 'database.sqlite'
        self.dbfile = path + '/' + db_file

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def read_db(self):
        try:
            con = sql.connect(self.dbfile)
            con.row_factory = self.dict_factory
            cur = con.cursor()
            cur.execute('SELECT * FROM to_disable')
            data = cur.fetchall()
        except sql.Error, e:
            print "Error {}:".format(e.args[0])
            sys.exit(1)
        return data

    def get_all_id(self):
        data = self.read_db()
        return [x['id'] for x in data]

    def add_log(self, user, message):
        curr_time = int(time.time())
        try:
            con = sql.connect(self.dbfile)
            cur = con.cursor()
            cur.execute("INSERT INTO logs(date, user, message) VALUES (%d, '%s', '%s')" % (curr_time, user, message))
            con.commit()
        except sql.Error, e:
            print "Error {}:".format(e.args[0])
            sys.exit(1)


def db_add_log(user, message):
    d = db()
    d.add_log(user, message)


def db_get_data():
    d = db()
    all_data = d.read_db()
    return all_data
