from database import db_get_data, db_add_log
from datetime import datetime as dt
from jnpr.junos import Device as JunDevice
from jnpr.junos.utils.config import Config
import jnpr.junos.exception as jexception
import logging
import yaml
import os
import sys

SQL_DB = 'database.sqlite'

path = os.path.dirname(os.path.abspath(__file__))

with open(path + '/config.yaml') as file:
    config_file = yaml.load(file, Loader=yaml.FullLoader)

logging.basicConfig(filename=path + '/' + config_file['log_file'],
                    format='%(asctime)s - %(message)s',
                    level=logging.WARNING)


class Executor(object):
    def __init__(self):
        path = os.path.dirname(os.path.abspath(__file__))

        with open(path + '/config.yaml') as file:
            config_file = yaml.load(file, Loader=yaml.FullLoader)

        if config_file['rtr_user']:
            self.username = config_file['rtr_user']
        else:
            logging.critical("Router user hasn't been defined - exit")
            sys.exit(1)

        if config_file['rtr_ssh_private_key']:
            self.ssh_private = config_file['rtr_ssh_private_key']
            self.password = False
        elif config_file['rtr_pass']:
            self.password = config_file['rtr_pass']
            self.ssh_private = False
        else:
            logging.critical("Password/private key hasn't been set - one of the option has to be set - exit")
            sys.exit(1)

    def _get_dev(self, router):
        if self.ssh_private:
            dev = JunDevice(host=router, user=self.username,
                            ssh_private_key_file=self.ssh_private)
        elif self.password:
            dev = JunDevice(host=router, user=self.username,
                            password=self.password)
        return dev

    def _deactivate(self, data, dev):
        ls = data['logicalsystem']
        iface, unit = data['iface'].split('.')

        try:
            dev.open()
        except jexception.ConnectAuthError:
            logging.critical("Problem with user %s authentication" % self.username)
            sys.exit(1)
        except jexception.ConnectRefusedError:
            logging.critical('Connection to the router has been refused')
            sys.exit(1)
        except jexception.ConnectTimeoutError:
            logging.critical('Connection to the router timeout')
            sys.exit(1)
        except jexception.ConnectError as err:
            logging.critical('Problem with connection to the router occured')
            logging.critical(err)
            sys.exit(1)

        try:
            with Config(dev, mode='private') as cu:
                if ls != '-':
                    command = 'deactivate logical-systems %s interfaces %s unit %s' % (ls, iface, unit)
                    cu.load(command, format='set')
                else:
                    command = 'deactivate interfaces %s unit %s' % (iface, unit)
                    cu.load(command, format='set')
                cu.commit()
        except jexception.PermissionError as err:
            logging.critical('Problem with access to configuration')
            logging.critical('Error message: ' + str(err))
        except jexception.CommitError as err:
            logging.critical('Problem with commit')
            logging.critical('Error message: ' + str(err))

    def get_iface_desc(self, router, iface):
        dev = self._get_dev(router)
        dev.open()
        rpc = dev.rpc.get_interface_information({'format': 'json'},
                                                interface_name=iface)
        return rpc['interface-information'][0]['logical-interface'][0]['description'][0]['data']

    def run(self):
        data = db_get_data()
        current_date = dt.now().strftime("%Y-%m-%d")
        for row in data:
            date = dt.fromtimestamp(row['date']).strftime("%Y-%m-%d")
            if date == current_date:
                db_add_log('executor', 'Deactivating service %s - router: %s - interface: %s' % (row['service_id'], row['router'], row['iface']))
                dev = self._get_dev(row['router'])

                self._deactivate(row, dev)


if __name__ == "__main__":
    e = Executor()
    e.run()
