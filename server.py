import datetime
import json
import time
from config import ConfigFile
from flask import Flask, render_template, url_for, request, redirect, flash
from flask_sqlalchemy import SQLAlchemy
from flask_user import current_user, login_required, roles_required, UserManager, UserMixin
from sqlalchemy.sql import asc, desc


class ConfigClass(object):
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

    SQLALCHEMY_DATABASE_URI = 'sqlite:///database.sqlite'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    USER_APP_NAME = "Flask-User QuickStart App"
    USER_ENABLE_EMAIL = False
    USER_ENABLE_USERNAME = True
    USER_REQUIRE_RETYPE_PASSWORD = False


def create_app():
    app = Flask(__name__,
                static_url_path='',
                static_folder='static')
    app.config.from_object(__name__+'.ConfigClass')

    db = SQLAlchemy(app)

    class User(db.Model, UserMixin):
        __tablename__ = 'users'
        id = db.Column(db.Integer, primary_key=True)
        active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

        username = db.Column(db.String(100, collation='NOCASE'), nullable=False, unique=True)
        password = db.Column(db.String(255), nullable=False, server_default='')
        email_confirmed_at = db.Column(db.DateTime())

        first_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
        last_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')

        roles = db.relationship('Role', secondary='user_roles')

    class Role(db.Model):
        __tablename__ = 'roles'
        id = db.Column(db.Integer(), primary_key=True)
        name = db.Column(db.String(50), unique=True)

    class UserRoles(db.Model):
        __tablename__ = 'user_roles'
        id = db.Column(db.Integer(), primary_key=True)
        user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
        role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))

    class ToDisable(db.Model):
        __tablename__ = 'to_disable'
        id = db.Column(db.Integer(), primary_key=True)
        service_id = db.Column(db.String(100), nullable=False, server_default='')
        router = db.Column(db.String(20), nullable=False, server_default='')
        iface = db.Column(db.String(50), nullable=False, server_default='')
        logicalsystem = db.Column(db.String(20), nullable=False, server_default='-')
        date = db.Column(db.Integer())

    class Logs(db.Model):
        __tablename__ = 'logs'
        id = db.Column(db.Integer(), primary_key=True)
        date = db.Column(db.Integer())
        user = db.Column(db.String(100, collation='NOCASE'))
        message = db.Column(db.String(255), nullable=False)

    db.create_all()

    user_manager = UserManager(app, db, User)

    if not User.query.filter(User.username == 'admin').first():
        user = User(
            username='admin',
            email_confirmed_at=datetime.datetime.utcnow(),
            password=user_manager.hash_password('Admin123'),
        )
        user.roles.append(Role(name='Admin'))
        db.session.add(user)
        db.session.commit()

    @app.template_filter('ctime')
    def timectime(s):
        return datetime.datetime.fromtimestamp(s).strftime("%Y-%m-%d")

    @app.template_filter('dtime')
    def timedtime(s):
        return datetime.datetime.fromtimestamp(s).strftime("%Y-%m-%d %H:%M")

    def add_new_log(user, message):
        curr_date = int(time.time())
        log = Logs(date=curr_date, user=user.username, message=message)
        db.session.add(log)
        db.session.commit()

    @app.route('/')
    @login_required
    def index():
        data = ToDisable.query.filter(ToDisable.date > int(time.time())).order_by(asc(ToDisable.date)).all()
        return render_template('index.html', data=data)

    @app.route('/login')
    def loginpage():
        return render_template('flask_user/register.html')

    @app.route('/members')
    @roles_required('Admin')
    def members():
        res = db.session.query(User, UserRoles, Role).filter(User.id == UserRoles.user_id).filter(Role.id == UserRoles.role_id).all()
        d = []
        for row in res:
            d.append({'username': row[0].username,
                      'first_name': row[0].first_name,
                      'last_name': row[0].last_name,
                      'role': row[2].name})
        out = sorted(d, key=lambda k: k['username'])
        return render_template('members.html', data=out)

    @app.route('/add', methods=['GET', 'POST'])
    @login_required
    def addrecord():
        if request.method == 'POST':
            data = json.loads(request.data)
            disable_date = data['disable_date']
            service_id = data['serviceID']
            router = data['rtr_name']
            iface = data['rtr_iface']
            logicalsystem = data['rtr_ls']
            if logicalsystem == '':
                logicalsystem = '-'
            user_date = int(time.mktime(time.strptime(disable_date + " 00:00:01", '%d-%m-%Y %H:%M:%S')))
            check = db.select([ToDisable]).where((ToDisable.service_id == service_id) &
                                                 (ToDisable.router == router) &
                                                 (ToDisable.iface == iface) &
                                                 (ToDisable.date == user_date))

            result = db.session.execute(check)
            row = result.fetchone()
            if not row:
                # print('ok brak w bazie - mozna dodac')
                d = ToDisable(service_id=service_id,
                              router=router,
                              iface=iface,
                              logicalsystem=logicalsystem,
                              date=user_date)
                db.session.add(d)
                db.session.commit()
                add_new_log(current_user, "New service to disable has been added - date of disable: %s, name: %s, router: %s, interface: %s, logical system: %s" % (disable_date, service_id, router, iface, logicalsystem))
            else:
                print('hehe juz w bazie - gtfo')
        return redirect(url_for('index'))

    @app.route('/add-new-user', methods=['GET', 'POST'])
    @login_required
    def addnewuser():
        if request.method == 'POST':
            data = json.loads(request.data)
            username = data['new_username']
            name = data['new_name']
            surname = data['new_surname']
            role = data['new_role']
            password = data['new_password']
            existing_user = User.query.filter_by(username=username).first()
            if existing_user is None:
                user = User(
                    username=username,
                    first_name=name,
                    last_name=surname,
                    email_confirmed_at=datetime.datetime.utcnow(),
                    password=user_manager.hash_password(password),
                )
                db.session.add(user)
                db.session.commit()
                r_user = User.query.filter_by(username=username).first()
                r_role = Role.query.filter_by(name=role).first()
                if not r_role:
                    db.session.add(Role(name=role))
                    db.session.commit()
                    r_role = Role.query.filter_by(name=role).first()

                user_role = UserRoles(user_id=r_user.id,
                                      role_id=r_role.id)
                db.session.add(user_role)
                db.session.commit()
                add_new_log(current_user, "New user has been added - username: %s" % (username))
            else:
                flash('A user already exists with that username.')
        return redirect(url_for('members'))

    @app.route('/del-user', methods=['GET', 'POST'])
    @login_required
    def del_user():
        if request.method == 'POST':
            data = json.loads(request.data)
            username = data['username']
            r_user = User.query.filter_by(username=username).first()
            db.session.delete(r_user)
            db.session.commit()
            add_new_log(current_user, "User has been deleted - username: %s" % (username))
        return redirect(url_for('members'))

    @app.route('/del-service', methods=['GET', 'POST'])
    @login_required
    def del_service():
        if request.method == 'POST':
            data = json.loads(request.data)
            service_id = data['service_id']
            r_service = ToDisable.query.filter_by(id=service_id).first()
            service_name = r_service.service_id
            db.session.delete(r_service)
            db.session.commit()
            add_new_log(current_user, "Service has been removed from the list - name: %s" % (service_name))
        return redirect(url_for('index'))

    @app.route('/logs')
    @login_required
    def logs():
        data = Logs.query.order_by(desc(Logs.date)).all()
        return render_template('logs.html', data=data)

    @app.route('/test')
    def testfunc():
        role = 'User'
        username = 'nowy1'
        r_user = User.query.filter_by(username=username).first()
        r_role = Role.query.filter_by(name=role).first()
        # print(r_user.id)
        # print(r_role.id)
        user_role = UserRoles(user_id=r_user.id,
                              role_id=r_role.id)
        db.session.add(user_role)
        db.session.commit()

        return 'done'

    return app


if __name__ == '__main__':
    app = create_app()
    cfg = ConfigFile()
    port = cfg.get_val('port')
    app.run(host='0.0.0.0', port=port, debug=True)
