import os
import yaml

class ConfigFile(object):
    def __init__(self):
        path = os.path.dirname(os.path.abspath(__file__))

        with open(path + '/config.yaml') as file:
            self.config_file = yaml.load(file, Loader=yaml.FullLoader)

    def get_val(self, key):
        return self.config_file[key]