from jnpr.junos import Device as JunDevice
from jnpr.junos.utils.config import Config
from lxml import etree
import jxmlease


dev = JunDevice(host='192.168.1.241', user='bgp', password='bgP123')
# dev = JunDevice(host='192.168.1.241', user='bgp', ssh_private_key_file='/Users/nelreth/python/juniper/bgp_rsa')
dev.open()
rpc = dev.rpc.get_interface_information({'format': 'json'}, interface_name="ge-0/0/2")
rpc_xml = etree.tostring(rpc, pretty_print=True, encoding='unicode')
dev.close()

xmlparser = jxmlease.Parser()
result = jxmlease.parse(rpc_xml)

print(result['interface-information']['physical-interface'][4]['logical-interface'][0]['description'])


"""
with Config(dev, mode='private') as cu:
    cu.load('set system services netconf traceoptions file test.log', format='set')
    cu.pdiff()
    cu.commit()
"""